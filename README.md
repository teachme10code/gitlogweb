1. Make a copy of the repository you want to monitor

        Ex: git clone https://my-awesome-project.git awesome-project-copy 

2. Edit the path to your repository copy and the location of your gitLogWeb folder in gitLog.sh

3. Run sh gitLog.sh to begin monitoring the repository copy. This will generate a commits.json file that will supply the data to your webpage

4. Start your webserver in the root of the gitLogWeb directory:

	Ex: http-server -p 8083

5. Nav to localhost:8083 in your browser to see the webpage