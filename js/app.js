"use strict";

var app = angular.module('app', [])
    .controller('CommitController', ['$scope', '$http', function($scope, $http) {
        $http.get('commits.json').success(function(commits) {
            //console.log(commits);
            $scope.commits = commits;
            $scope.$watch('commits', function() {
            });
        });
    }]);