#!/usr/bin/env bash

cd /Users/$USER/<path-to-repository>


d=('date')
t=(`date +%H`)

while [ $t -le 18 ] #stops at 6pm
do
    git pull
    $d
    echo '\n'
    git log \
	    --pretty=format:'{%n  "commit": "%H",%n  "author": "%an",%n  "date": "%cD",%n  "message": "%f"%n},' --since="two days ago"\
	    $@ | \
	    perl -pe 'BEGIN{print "["}; END{print "]\n"}' | \
	    perl -pe 's/},]/}]/' > /Users/$USER/gitLogWeb/commits.json \

	sleep 600
done
